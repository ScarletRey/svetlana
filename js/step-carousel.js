﻿$(document).ready(function() {
  var slides = $(".step-width").children(".step-slide");
  var width = $(".step-width").width();
  var widthSlide = $(".step-slide").innerWidth();
  var pad = widthSlide - $(".step-slide").width();
  var offset = slides.length * widthSlide;
  $(".step-width").css('width',offset);
  var x = 0;
  var y = width;
  if (slides.length <= 3) {
    $(".step-right").hide();
    $(".step-left").hide();
  }
  $(".step-right").click(function(){
    if (width < (offset-pad)) {
      width += widthSlide;
      x += widthSlide;
      $(".step-width").css("transform","translate3d(-"+x+"px, 0px, 0px)");
    } else { while (width > y) {
      width -= widthSlide;
      x -= widthSlide;
      $(".step-width").css("transform","translate3d(-"+x+"px, 0px, 0px)");
    }}
  });
  $(".step-left").click(function(){
  if (width > y) {
    width -= widthSlide;
    x -= widthSlide;
    $(".step-width").css("transform","translate3d(-"+x+"px, 0px, 0px)");
  } else {while (width < (offset-pad)) {
    width += widthSlide;
    x += widthSlide;
    $(".step-width").css("transform","translate3d(-"+x+"px, 0px, 0px)");
  }}
});
});
